package com.bkozyrev.alphabanktest.Server;

import com.bkozyrev.alphabanktest.Model.Rss;

import retrofit.http.GET;
import rx.Observable;

public interface AlphaAPI {

    String BASE_URL = "https://alfabank.ru";

    @GET("/_/rss/_rss.html?subtype=3&category=2&city=21")
    Observable<Rss> getRss();
}