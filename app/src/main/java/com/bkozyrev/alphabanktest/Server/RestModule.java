package com.bkozyrev.alphabanktest.Server;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.SimpleXMLConverter;

@Module
@Singleton
public class RestModule {

    @Provides
    @Singleton
    AlphaAPI provideAlphaApi() { return provideRetrofitBuilder().build().create(AlphaAPI.class); }

    @Provides
    RestAdapter.Builder provideRetrofitBuilder() {

        OkHttpClient client = new OkHttpClient();

        return new RestAdapter.Builder()
                .setEndpoint(AlphaAPI.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new SimpleXMLConverter())
                .setClient(new OkClient(client));
    }
}


