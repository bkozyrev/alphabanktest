package com.bkozyrev.alphabanktest.MVP.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bkozyrev.alphabanktest.MVP.Presenter.MainActivityPresenter;
import com.bkozyrev.alphabanktest.MVP.Presenter.MainActivityPresenterImpl;
import com.bkozyrev.alphabanktest.MVP.View.MainActivityView;
import com.bkozyrev.alphabanktest.MVP.View.MainActivityViewImpl;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.MainActivityViewHolder;
import com.bkozyrev.alphabanktest.R;

public class MainActivity extends AppCompatActivity {

    private MainActivityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final MainActivityViewHolder viewHolder = new MainActivityViewHolder(
                (DrawerLayout) findViewById(R.id.drawer_layout),
                (NavigationView) findViewById(R.id.navigation_view),
                (Toolbar) findViewById(R.id.toolbar)
        );

        MainActivityView view = new MainActivityViewImpl(viewHolder);

        presenter = new MainActivityPresenterImpl(this, view);

        presenter.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return presenter.onOptionsItemSelected(item);
    }
}
