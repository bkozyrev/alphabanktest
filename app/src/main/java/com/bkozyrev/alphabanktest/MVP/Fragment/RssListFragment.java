package com.bkozyrev.alphabanktest.MVP.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bkozyrev.alphabanktest.Dagger.InjectorClass;
import com.bkozyrev.alphabanktest.MVP.Model.RssListFragmentModel;
import com.bkozyrev.alphabanktest.MVP.Model.RssListFragmentModelImpl;
import com.bkozyrev.alphabanktest.MVP.Presenter.RssListFragmentPresenter;
import com.bkozyrev.alphabanktest.MVP.Presenter.RssListFragmentPresenterImpl;
import com.bkozyrev.alphabanktest.MVP.View.RssListFragmentView;
import com.bkozyrev.alphabanktest.MVP.View.RssListFragmentViewImpl;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.RssListFragmentViewHolder;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.Server.AlphaAPI;

import javax.inject.Inject;

public class RssListFragment extends Fragment {

    public static final String RSS_ITEMS_KEY = "rss_items";

    private RssListFragmentPresenter presenter;
    private Context context;

    @Inject
    AlphaAPI alphaAPI;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
        presenter.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InjectorClass.getRestComponent().inject(this);
        return inflater.inflate(R.layout.fragment_rss_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        RssListFragmentViewHolder viewHolder = new RssListFragmentViewHolder(
                (TextView) view.findViewById(R.id.empty_list_message),
                (RecyclerView) view.findViewById(R.id.list),
                (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh)
        );

        RssListFragmentView fragmentView = new RssListFragmentViewImpl(viewHolder);

        RssListFragmentModel model = new RssListFragmentModelImpl(alphaAPI);

        presenter = new RssListFragmentPresenterImpl(this, context, fragmentView, model);
        presenter.onViewCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
