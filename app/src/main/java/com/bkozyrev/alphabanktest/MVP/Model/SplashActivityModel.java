package com.bkozyrev.alphabanktest.MVP.Model;

import com.bkozyrev.alphabanktest.Model.Rss;

import rx.Observable;

public interface SplashActivityModel {

    Observable<String> changeText();

    Observable<Rss> request();
}
