package com.bkozyrev.alphabanktest.MVP.View;

import android.content.Context;
import android.support.design.widget.NavigationView;

import com.bkozyrev.alphabanktest.MVP.Activity.MainActivity;

public interface MainActivityView {

    void setNavigationView(Context context, NavigationView.OnNavigationItemSelectedListener listener);

    void setUpToolbar(MainActivity activity);

    void openDrawer(int gravity);

    void closeDrawer(int gravity);

    boolean isDrawerOpen(int gravity);
}
