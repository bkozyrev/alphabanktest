package com.bkozyrev.alphabanktest.MVP.Model;

import com.bkozyrev.alphabanktest.Model.Rss;
import com.bkozyrev.alphabanktest.Server.AlphaAPI;

import rx.Observable;

public class RssListFragmentModelImpl implements RssListFragmentModel {

    private AlphaAPI alphaAPI;

    public RssListFragmentModelImpl(AlphaAPI alphaAPI) {
        this.alphaAPI = alphaAPI;
    }

    @Override
    public Observable<Rss> request() {
        return alphaAPI.getRss();
    }
}
