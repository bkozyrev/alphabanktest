package com.bkozyrev.alphabanktest.MVP.View;

import android.graphics.drawable.Drawable;
import android.view.View;

public interface SplashActivityView {

    void setClickListener(View.OnClickListener listener);

    void setFlagDrawable(Drawable drawable);

    void enableProgressBar();

    void disableProgressBar();

    void setButtonText(String text);

    void enableLoginButton();

    void disableLoginButton();
}
