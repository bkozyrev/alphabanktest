package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssPagerAdapter;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssListFragment;
import com.bkozyrev.alphabanktest.MVP.Model.RssDetailsActivityModel;
import com.bkozyrev.alphabanktest.MVP.View.RssDetailsActivityView;
import com.bkozyrev.alphabanktest.Model.RssItem;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.MVP.Activity.RssDetailsActivity;
import com.bkozyrev.alphabanktest.Utils.PreferenceHelper;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

import java.util.ArrayList;
import java.util.List;

public class RssDetailsActivityPresenterImpl implements RssDetailsActivityPresenter, View.OnClickListener,
        ViewPager.OnPageChangeListener {

    private RssDetailsActivity activity;
    private Context context;
    private RssDetailsActivityView activityView;
    private RssDetailsActivityModel model;
    private ArrayList<RssItem> items, favItems;
    private SimpleFacebook simpleFacebook;

    public RssDetailsActivityPresenterImpl(RssDetailsActivity activity, Context context, RssDetailsActivityView view,
                                           RssDetailsActivityModel model) {
        this.activity = activity;
        this.context = context;
        this.activityView = view;
        this.model = model;
        this.simpleFacebook = SimpleFacebook.getInstance(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        favItems = PreferenceHelper.getFavList(context);
        if (favItems == null)
            favItems = new ArrayList<>();

        items = new ArrayList<>();
        int startPosition = 0;
        Intent intent = activity.getIntent();
        if (intent != null && intent.hasExtra(RssListFragment.RSS_ITEMS_KEY))
            items = intent.getParcelableArrayListExtra(RssListFragment.RSS_ITEMS_KEY);
        if (intent != null && intent.hasExtra(RssDetailsActivity.RSS_ITEMS_START_POS))
            startPosition = intent.getIntExtra(RssDetailsActivity.RSS_ITEMS_START_POS, 0);

        setFavBtn(items.get(startPosition));
        activityView.setClickListener(this);
        activityView.setUpViewPager(this, new RssPagerAdapter(context, items), startPosition);
    }

    @Override
    public void onResume() {
        simpleFacebook = SimpleFacebook.getInstance(activity);
    }

    private void setFavBtn(RssItem currentItem) {
        boolean isFav = false;
        for (RssItem favItem: favItems) {
            if (favItem.equals(currentItem))
                isFav = true;
        }

        activityView.setFavButton(context, isFav);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fav_btn:
                //Лучшим вариантом было бы реализовать базу данных для хранения избранных новостей
                int currentPosition = activityView.getPagerCurrentItem();
                if (activityView.isFavBtnActive()) {
                    favItems.remove(items.get(currentPosition));
                    PreferenceHelper.removeRssItemFromFav(context, items.get(currentPosition));
                    activityView.setFavButton(context, false);
                } else {
                    favItems.add(items.get(currentPosition));
                    PreferenceHelper.saveRssItemToFav(context, items.get(currentPosition));
                    activityView.setFavButton(context, true);
                }
                break;
            case R.id.share_btn:
                if (simpleFacebook.getAccessToken() == null) {
                    model.signInFacebook(simpleFacebook, new OnLoginListener() {
                        @Override
                        public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                            shareRssToFB(items.get(activityView.getPagerCurrentItem()));
                        }

                        @Override
                        public void onCancel() {

                        }

                        @Override
                        public void onException(Throwable throwable) {
                            Log.e(activity.getClass().getSimpleName(), throwable.getMessage());
                        }

                        @Override
                        public void onFail(String reason) {

                        }
                    });
                } else {
                    shareRssToFB(items.get(activityView.getPagerCurrentItem()));
                }
                break;
        }
    }

    private void shareRssToFB(RssItem rssItem) {
        model.shareToFacebook(simpleFacebook, rssItem, new OnPublishListener() {
            @Override
            public void onComplete(String response) {
                super.onComplete(response);
            }

            @Override
            public void onFail(String reason) {
                super.onFail(reason);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        simpleFacebook.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setFavBtn(items.get(position));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                activity.onBackPressed();
                break;
        }
        return false;
    }
}
