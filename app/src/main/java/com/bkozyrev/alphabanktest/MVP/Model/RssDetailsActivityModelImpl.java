package com.bkozyrev.alphabanktest.MVP.Model;

import com.bkozyrev.alphabanktest.Model.RssItem;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

public class RssDetailsActivityModelImpl implements RssDetailsActivityModel {

    @Override
    public void signInFacebook(SimpleFacebook simpleFacebook, OnLoginListener listener) {
        simpleFacebook.login(listener);
    }

    @Override
    public void shareToFacebook(SimpleFacebook simpleFacebook, RssItem rssItem, OnPublishListener listener) {
        Feed feed = new Feed.Builder()
                .setMessage(rssItem.getDescription())
                .setName(rssItem.getTitle())
                .setCaption(rssItem.getTitle())
                .setDescription(rssItem.getDescription())
                .setLink(rssItem.getLink())
                .build();

        simpleFacebook.publish(feed, true, new OnPublishListener() {
            @Override
            public void onComplete(String response) {
                super.onComplete(response);
            }

            @Override
            public void onFail(String reason) {
                super.onFail(reason);
            }
        });
    }
}
