package com.bkozyrev.alphabanktest.MVP.View;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssPagerAdapter;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.RssDetailsActivityViewHolder;
import com.bkozyrev.alphabanktest.R;

public class RssDetailsActivityViewImpl implements RssDetailsActivityView {

    private RssDetailsActivityViewHolder viewHolder;

    public RssDetailsActivityViewImpl(RssDetailsActivityViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    @Override
    public void setClickListener(View.OnClickListener listener) {
        viewHolder.favBtn.setOnClickListener(listener);
        viewHolder.shareBtn.setOnClickListener(listener);
    }

    @Override
    public void setUpViewPager(ViewPager.OnPageChangeListener listener, RssPagerAdapter adapter, int startPosition) {
        viewHolder.viewPager.addOnPageChangeListener(listener);
        viewHolder.viewPager.setAdapter(adapter);
        viewHolder.viewPager.setCurrentItem(startPosition);
    }

    @Override
    public void setFavButton(Context context, boolean isFav) {
        if (isFav) {
            viewHolder.favBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_36dp));
            viewHolder.favBtn.setTag("fav");
        } else {
            viewHolder.favBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_36dp));
            viewHolder.favBtn.setTag("not fav");
        }
    }

    @Override
    public boolean isFavBtnActive() {
        return viewHolder.favBtn.getTag().equals("fav");
    }

    @Override
    public int getPagerCurrentItem() {
        return viewHolder.viewPager.getCurrentItem();
    }
}
