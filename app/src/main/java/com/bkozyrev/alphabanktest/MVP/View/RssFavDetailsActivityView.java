package com.bkozyrev.alphabanktest.MVP.View;

import com.bkozyrev.alphabanktest.Adapters.RssPagerAdapter;

public interface RssFavDetailsActivityView {

    void setUpViewPager(RssPagerAdapter adapter, int startPosition);
}
