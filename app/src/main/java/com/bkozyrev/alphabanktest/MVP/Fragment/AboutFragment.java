package com.bkozyrev.alphabanktest.MVP.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bkozyrev.alphabanktest.R;

public class AboutFragment extends Fragment {

    private TextView tvName, tvCV, tvProject;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        tvName = (TextView) view.findViewById(R.id.tv_name);
        tvCV = (TextView) view.findViewById(R.id.tv_cv);
        tvProject = (TextView) view.findViewById(R.id.tv_project);

        tvCV.setMovementMethod(LinkMovementMethod.getInstance());
        tvProject.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
