package com.bkozyrev.alphabanktest.MVP.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bkozyrev.alphabanktest.MVP.Presenter.RssFavListFragmentPresenter;
import com.bkozyrev.alphabanktest.MVP.Presenter.RssFavListFragmentPresenterImpl;
import com.bkozyrev.alphabanktest.MVP.View.RssFavListFragmentView;
import com.bkozyrev.alphabanktest.MVP.View.RssFavListFragmentViewImpl;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.RssFavListFragmentViewHolder;
import com.bkozyrev.alphabanktest.R;

public class RssFavListFragment extends Fragment {

    public static final String RSS_ITEMS_KEY = "rss_items";

    private RssFavListFragmentPresenter presenter;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
        presenter.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rss_fav_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        RssFavListFragmentViewHolder viewHolder = new RssFavListFragmentViewHolder(
                (RecyclerView) view.findViewById(R.id.list)
        );

        RssFavListFragmentView fragmentView = new RssFavListFragmentViewImpl(viewHolder);

        presenter = new RssFavListFragmentPresenterImpl(context, this, fragmentView);
        presenter.onViewCreated(savedInstanceState);
    }
}
