package com.bkozyrev.alphabanktest.MVP.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.bkozyrev.alphabanktest.Dagger.InjectorClass;
import com.bkozyrev.alphabanktest.MVP.Model.SplashActivityModel;
import com.bkozyrev.alphabanktest.MVP.Model.SplashActivityModelImpl;
import com.bkozyrev.alphabanktest.MVP.Presenter.SplashActivityPresenter;
import com.bkozyrev.alphabanktest.MVP.Presenter.SplashActivityPresenterImpl;
import com.bkozyrev.alphabanktest.MVP.View.SplashActivityView;
import com.bkozyrev.alphabanktest.MVP.View.SplashActivityViewImpl;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.SplashActivityViewHolder;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.Server.AlphaAPI;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity {

    private SplashActivityPresenter presenter;

    @Inject
    AlphaAPI alphaAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        InjectorClass.getRestComponent().inject(this);

        final SplashActivityViewHolder splashActivityViewHolder = new SplashActivityViewHolder(
                (EditText) findViewById(R.id.login_edit_text),
                (AppCompatButton) findViewById(R.id.login_button),
                (ProgressBar) findViewById(R.id.progress_bar)
        );

        SplashActivityView view = new SplashActivityViewImpl(splashActivityViewHolder);

        SplashActivityModel model = new SplashActivityModelImpl(alphaAPI, splashActivityViewHolder);

        presenter = new SplashActivityPresenterImpl(this, model, view);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }
}
