package com.bkozyrev.alphabanktest.MVP.View;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssRecyclerViewAdapter;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.RssFavListFragmentViewHolder;

public class RssFavListFragmentViewImpl implements RssFavListFragmentView {

    private RssFavListFragmentViewHolder viewHolder;

    public RssFavListFragmentViewImpl(RssFavListFragmentViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    @Override
    public void setUpRecyclerView(Context context, RssRecyclerViewAdapter adapter) {
        viewHolder.list.setLayoutManager(new LinearLayoutManager(context));
        viewHolder.list.setAdapter(adapter);
    }

    @Override
    public int getRecyclerViewChildAdapterPosition(View view) {
        return viewHolder.list.getChildAdapterPosition(view);
    }
}
