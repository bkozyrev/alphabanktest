package com.bkozyrev.alphabanktest.MVP.View;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssRecyclerViewAdapter;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.RssListFragmentViewHolder;

public class RssListFragmentViewImpl implements RssListFragmentView {

    private RssListFragmentViewHolder viewHolder;

    public RssListFragmentViewImpl(RssListFragmentViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    @Override
    public void setRefreshListener(SwipeRefreshLayout.OnRefreshListener listener) {
        viewHolder.swipeRefreshLayout.setOnRefreshListener(listener);
    }

    @Override
    public void onStartRefresh() {
        viewHolder.swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onStopRefresh() {
        viewHolder.swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setUpRecyclerView(Context context, RssRecyclerViewAdapter adapter) {
        viewHolder.list.setLayoutManager(new LinearLayoutManager(context));
        viewHolder.list.setAdapter(adapter);
    }

    @Override
    public int getRecyclerViewChildAdapterPosition(View view) {
        return viewHolder.list.getChildAdapterPosition(view);
    }

    @Override
    public void showEmptyListMessage() {
        viewHolder.emptyListMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyListMessage() {
        viewHolder.emptyListMessage.setVisibility(View.GONE);
    }
}
