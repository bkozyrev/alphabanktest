package com.bkozyrev.alphabanktest.MVP.View;

import android.content.Context;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssRecyclerViewAdapter;

public interface RssFavListFragmentView {

    void setUpRecyclerView(Context context, RssRecyclerViewAdapter adapter);

    int getRecyclerViewChildAdapterPosition(View view);
}
