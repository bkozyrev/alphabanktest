package com.bkozyrev.alphabanktest.MVP.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.bkozyrev.alphabanktest.MVP.Presenter.RssFavDetailsActivityPresenterImpl;
import com.bkozyrev.alphabanktest.MVP.View.RssFavDetailsActivityView;
import com.bkozyrev.alphabanktest.MVP.View.RssFavDetailsActivityViewImpl;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.RssFavDetailsActivityViewHolder;
import com.bkozyrev.alphabanktest.R;

public class RssFavDetailsActivity extends AppCompatActivity {

    public static final String RSS_ITEMS_START_POS = "rss_items_pos";

    private RssFavDetailsActivityPresenterImpl presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_fav_details);

        RssFavDetailsActivityViewHolder viewHolder = new RssFavDetailsActivityViewHolder(
                (ViewPager) findViewById(R.id.view_pager)
        );

        RssFavDetailsActivityView view = new RssFavDetailsActivityViewImpl(viewHolder);

        presenter = new RssFavDetailsActivityPresenterImpl(this, getApplicationContext(), view);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return presenter.onOptionsItemSelected(item);
    }
}
