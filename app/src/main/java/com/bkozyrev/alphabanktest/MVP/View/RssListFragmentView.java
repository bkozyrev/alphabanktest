package com.bkozyrev.alphabanktest.MVP.View;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssRecyclerViewAdapter;

public interface RssListFragmentView {

    void setRefreshListener(SwipeRefreshLayout.OnRefreshListener listener);

    void onStartRefresh();

    void onStopRefresh();

    void setUpRecyclerView(Context context, RssRecyclerViewAdapter adapter);

    int getRecyclerViewChildAdapterPosition(View view);

    void showEmptyListMessage();

    void hideEmptyListMessage();
}
