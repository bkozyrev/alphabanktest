package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.os.Bundle;
import android.view.MenuItem;

public interface MainActivityPresenter {

    void onCreate(Bundle savedInstanceState);

    void onPause();

    boolean onOptionsItemSelected(MenuItem item);
}
