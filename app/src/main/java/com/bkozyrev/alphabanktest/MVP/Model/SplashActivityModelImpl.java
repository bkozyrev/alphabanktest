package com.bkozyrev.alphabanktest.MVP.Model;

import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.SplashActivityViewHolder;
import com.bkozyrev.alphabanktest.Model.Rss;
import com.bkozyrev.alphabanktest.Server.AlphaAPI;
import com.jakewharton.rxbinding.widget.RxTextView;

import rx.Observable;
import rx.functions.Func1;

public class SplashActivityModelImpl implements SplashActivityModel {

    private SplashActivityViewHolder viewHolder;
    private AlphaAPI alphaAPI;

    public SplashActivityModelImpl(AlphaAPI alphaAPI, SplashActivityViewHolder viewHolder) {
        this.viewHolder = viewHolder;
        this.alphaAPI = alphaAPI;
    }

    @Override
    public Observable<String> changeText() {
        return RxTextView.textChanges(viewHolder.etLogin)
                .map(new Func1<CharSequence, String>() {
                    @Override
                    public String call(CharSequence charSequence) {
                        return charSequence.toString();
                    }
                });
    }

    @Override
    public Observable<Rss> request() {
        return alphaAPI.getRss();
    }
}
