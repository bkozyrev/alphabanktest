package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.view.MenuItem;

import com.bkozyrev.alphabanktest.MVP.Fragment.AboutFragment;
import com.bkozyrev.alphabanktest.MVP.View.MainActivityView;
import com.bkozyrev.alphabanktest.MVP.Activity.MainActivity;
import com.bkozyrev.alphabanktest.Model.RssItem;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssFavListFragment;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssListFragment;
import com.bkozyrev.alphabanktest.Utils.PreferenceHelper;

import java.util.ArrayList;

public class MainActivityPresenterImpl implements MainActivityPresenter, NavigationView.OnNavigationItemSelectedListener {

    private MainActivity activity;
    private Context context;
    private MainActivityView view;
    private ArrayList<RssItem> items;

    public MainActivityPresenterImpl(MainActivity activity, MainActivityView view) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.view = view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setNavigationView(context, this);
        view.setUpToolbar(activity);

        items = new ArrayList<>();
        Intent intent = activity.getIntent();
        if (intent != null && intent.hasExtra(RssListFragment.RSS_ITEMS_KEY))
            items = intent.getParcelableArrayListExtra(RssListFragment.RSS_ITEMS_KEY);

        setSelectedItem(R.id.item_header_1);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        setSelectedItem(item.getItemId());
        item.setChecked(true);
        return true;
    }

    private void setSelectedItem(int menuItemId) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        switch (menuItemId) {
            case R.id.item_header_1:
                fragment = new RssListFragment();
                bundle.putParcelableArrayList(RssListFragment.RSS_ITEMS_KEY, items);
                break;
            case R.id.item_header_2:
                fragment = new RssFavListFragment();
                bundle.putParcelableArrayList(RssListFragment.RSS_ITEMS_KEY, PreferenceHelper.getFavList(context));
                break;
            case R.id.item_header_3:
                fragment = new AboutFragment();
                break;
            default:
                break;
        }

        if (fragment != null)
            showFragment(R.id.container, fragment, true, null, bundle);

        view.closeDrawer(GravityCompat.START);
    }

    private void showFragment(int container, Fragment fragment, boolean addToBackStack, String name, Bundle arguments) {
        if (arguments != null)
            fragment.setArguments(arguments);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(container, fragment, name);

        if (addToBackStack)
            fragmentTransaction.addToBackStack(name == null || name.isEmpty() ? null : name);

        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!view.isDrawerOpen(GravityCompat.START)) {
                    view.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return false;
    }

    @Override
    public void onPause() {

    }
}
