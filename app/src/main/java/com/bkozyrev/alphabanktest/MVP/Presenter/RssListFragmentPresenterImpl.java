package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssRecyclerViewAdapter;
import com.bkozyrev.alphabanktest.MVP.Model.RssListFragmentModel;
import com.bkozyrev.alphabanktest.MVP.View.RssListFragmentView;
import com.bkozyrev.alphabanktest.Model.Rss;
import com.bkozyrev.alphabanktest.Model.RssItem;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.MVP.Activity.RssDetailsActivity;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssListFragment;
import com.bkozyrev.alphabanktest.RssService;

import java.util.ArrayList;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RssListFragmentPresenterImpl implements RssListFragmentPresenter, RecyclerView.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private RssListFragment fragment;
    private BroadcastReceiver rssReceiver;
    private RssListFragmentView fragmentView;
    private RssListFragmentModel model;
    private ArrayList<RssItem> items;
    private RssRecyclerViewAdapter adapter;
    private Subscription apiSubscription;

    public RssListFragmentPresenterImpl(RssListFragment fragment, Context context, RssListFragmentView view,
                                        RssListFragmentModel model) {
        this.fragment = fragment;
        this.context = context;
        this.fragmentView = view;
        this.model = model;
    }

    @Override
    public void onDetach() {
        context = null;
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        Bundle bundle = fragment.getArguments();
        if (bundle != null && bundle.containsKey(RssListFragment.RSS_ITEMS_KEY))
            items = bundle.getParcelableArrayList(RssListFragment.RSS_ITEMS_KEY);

        if (items == null) {
            items = new ArrayList<>();
        }

        if (items.size() == 0) {
            fragmentView.showEmptyListMessage();
        }

        adapter = new RssRecyclerViewAdapter(context, items, this);
        fragmentView.setUpRecyclerView(context, adapter);
        fragmentView.setRefreshListener(this);
        setUpRssReceiver();
    }

    private void setUpRssReceiver() {
        rssReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                items = intent.getParcelableArrayListExtra(RssListFragment.RSS_ITEMS_KEY);
                if (fragment != null && fragment.isVisible()) {
                    fragmentView.hideEmptyListMessage();
                    adapter.updateRss(items);
                }
            }
        };

        LocalBroadcastManager.getInstance(context).registerReceiver(rssReceiver, new IntentFilter(RssService.ACTION_RSS_UPDATE));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rss_item_container:
                Intent intent = new Intent(context, RssDetailsActivity.class);
                intent.putParcelableArrayListExtra(RssListFragment.RSS_ITEMS_KEY, items);
                intent.putExtra(RssDetailsActivity.RSS_ITEMS_START_POS,
                        fragmentView.getRecyclerViewChildAdapterPosition(view));
                fragment.getActivity().startActivity(intent);
                break;
        }
    }

    @Override
    public void onRefresh() {
        rssGetRequest();
    }

    private void rssGetRequest() {
        apiSubscription = model.request()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Rss>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragmentView.onStopRefresh();
                    }

                    @Override
                    public void onNext(Rss rss) {
                        fragmentView.onStopRefresh();
                        fragmentView.hideEmptyListMessage();
                        if (rss != null) {
                            items = rss.getChannel().getRssItems();
                            adapter.updateRss(items);
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        if (apiSubscription != null && !apiSubscription.isUnsubscribed()) {
            apiSubscription.unsubscribe();
        }

        LocalBroadcastManager.getInstance(context).unregisterReceiver(rssReceiver);
    }
}
