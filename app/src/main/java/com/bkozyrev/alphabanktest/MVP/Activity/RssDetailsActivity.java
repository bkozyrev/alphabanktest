package com.bkozyrev.alphabanktest.MVP.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageButton;

import com.bkozyrev.alphabanktest.MVP.Model.RssDetailsActivityModel;
import com.bkozyrev.alphabanktest.MVP.Model.RssDetailsActivityModelImpl;
import com.bkozyrev.alphabanktest.MVP.Presenter.RssDetailsActivityPresenter;
import com.bkozyrev.alphabanktest.MVP.Presenter.RssDetailsActivityPresenterImpl;
import com.bkozyrev.alphabanktest.MVP.View.RssDetailsActivityView;
import com.bkozyrev.alphabanktest.MVP.View.RssDetailsActivityViewImpl;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.RssDetailsActivityViewHolder;
import com.bkozyrev.alphabanktest.R;

public class RssDetailsActivity extends AppCompatActivity {

    public static final String RSS_ITEMS_START_POS = "rss_items_pos";

    private RssDetailsActivityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_details);

        RssDetailsActivityViewHolder viewHolder = new RssDetailsActivityViewHolder(
                (ViewPager) findViewById(R.id.view_pager),
                (ImageButton) findViewById(R.id.fav_btn),
                (ImageButton) findViewById(R.id.share_btn)
        );

        RssDetailsActivityView view = new RssDetailsActivityViewImpl(viewHolder);

        RssDetailsActivityModel model = new RssDetailsActivityModelImpl();

        presenter = new RssDetailsActivityPresenterImpl(this, getApplicationContext(), view, model);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return presenter.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }
}
