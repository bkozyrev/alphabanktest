package com.bkozyrev.alphabanktest.MVP.View.ViewHolder;

import android.support.v7.widget.AppCompatButton;
import android.widget.EditText;
import android.widget.ProgressBar;

public class SplashActivityViewHolder {

    public EditText etLogin;
    public AppCompatButton btnLogin;
    public ProgressBar progressBar;

    public SplashActivityViewHolder(EditText etLogin, AppCompatButton btnLogin, ProgressBar progressBar) {
        this.etLogin = etLogin;
        this.btnLogin = btnLogin;
        this.progressBar = progressBar;
    }
}
