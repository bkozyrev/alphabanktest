package com.bkozyrev.alphabanktest.MVP.View.ViewHolder;

import android.support.v7.widget.RecyclerView;

public class RssFavListFragmentViewHolder {

    public RecyclerView list;

    public RssFavListFragmentViewHolder(RecyclerView list) {
        this.list = list;
    }
}
