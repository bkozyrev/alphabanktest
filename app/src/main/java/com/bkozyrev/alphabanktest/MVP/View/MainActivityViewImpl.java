package com.bkozyrev.alphabanktest.MVP.View;

import android.content.Context;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;

import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.MainActivityViewHolder;
import com.bkozyrev.alphabanktest.MVP.Activity.MainActivity;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.Utils.DividerItemDecoration;

public class MainActivityViewImpl implements MainActivityView {

    private MainActivityViewHolder viewHolder;

    public MainActivityViewImpl(MainActivityViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    @Override
    public void setNavigationView(Context context, NavigationView.OnNavigationItemSelectedListener listener) {
        NavigationMenuView navMenuView = (NavigationMenuView) viewHolder.navigationView.getChildAt(0);
        navMenuView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST, false, true));
        viewHolder.navigationView.setCheckedItem(R.id.item_header_1);
        viewHolder.navigationView.setNavigationItemSelectedListener(listener);
    }

    @Override
    public void setUpToolbar(MainActivity activity) {
        viewHolder.toolbar.setTitleTextColor(ContextCompat.getColor(activity.getApplicationContext(), android.R.color.white));
        activity.setSupportActionBar(viewHolder.toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        }
    }

    @Override
    public void openDrawer(int gravity) {
        viewHolder.drawerLayout.openDrawer(gravity);
    }

    @Override
    public void closeDrawer(int gravity) {
        viewHolder.drawerLayout.closeDrawer(gravity);
    }

    @Override
    public boolean isDrawerOpen(int gravity) {
        return viewHolder.drawerLayout.isDrawerOpen(gravity);
    }
}
