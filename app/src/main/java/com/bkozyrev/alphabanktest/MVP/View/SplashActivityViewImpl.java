package com.bkozyrev.alphabanktest.MVP.View;

import android.graphics.drawable.Drawable;
import android.view.View;

import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.SplashActivityViewHolder;

public class SplashActivityViewImpl implements SplashActivityView {

    private SplashActivityViewHolder viewHolder;

    public SplashActivityViewImpl(SplashActivityViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    @Override
    public void setClickListener(View.OnClickListener listener) {
        viewHolder.btnLogin.setOnClickListener(listener);
    }

    @Override
    public void setFlagDrawable(Drawable drawable) {
        viewHolder.etLogin.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
    }

    @Override
    public void enableProgressBar() {
        viewHolder.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void disableProgressBar() {
        viewHolder.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setButtonText(String text) {
        viewHolder.btnLogin.setText(text);
    }

    @Override
    public void enableLoginButton() {
        viewHolder.btnLogin.setEnabled(true);
    }

    @Override
    public void disableLoginButton() {
        viewHolder.btnLogin.setEnabled(false);
    }
}
