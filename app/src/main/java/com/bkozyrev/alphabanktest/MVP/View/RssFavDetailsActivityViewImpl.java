package com.bkozyrev.alphabanktest.MVP.View;

import com.bkozyrev.alphabanktest.Adapters.RssPagerAdapter;
import com.bkozyrev.alphabanktest.MVP.View.ViewHolder.RssFavDetailsActivityViewHolder;

public class RssFavDetailsActivityViewImpl implements RssFavDetailsActivityView {

    private RssFavDetailsActivityViewHolder viewHolder;

    public RssFavDetailsActivityViewImpl(RssFavDetailsActivityViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }

    @Override
    public void setUpViewPager(RssPagerAdapter adapter, int startPosition) {
        viewHolder.viewPager.setAdapter(adapter);
        viewHolder.viewPager.setCurrentItem(startPosition);
    }
}
