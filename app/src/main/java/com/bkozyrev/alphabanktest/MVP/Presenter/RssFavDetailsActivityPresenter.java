package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.os.Bundle;
import android.view.MenuItem;

public interface RssFavDetailsActivityPresenter {

    void onCreate(Bundle savedInstanceState);

    boolean onOptionsItemSelected(MenuItem item);
}
