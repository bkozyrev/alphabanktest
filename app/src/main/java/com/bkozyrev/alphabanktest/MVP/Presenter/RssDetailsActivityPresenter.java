package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

public interface RssDetailsActivityPresenter {

    void onCreate(Bundle savedInstanceState);

    void onResume();

    boolean onOptionsItemSelected(MenuItem item);

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
