package com.bkozyrev.alphabanktest.MVP.Model;

import com.bkozyrev.alphabanktest.Model.RssItem;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

public interface RssDetailsActivityModel {

    void signInFacebook(SimpleFacebook simpleFacebook, OnLoginListener listener);

    void shareToFacebook(SimpleFacebook simpleFacebook, RssItem rssItem, OnPublishListener listener);
}
