package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssRecyclerViewAdapter;
import com.bkozyrev.alphabanktest.MVP.View.RssFavListFragmentView;
import com.bkozyrev.alphabanktest.Model.RssItem;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.MVP.Activity.RssFavDetailsActivity;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssFavListFragment;

import java.util.ArrayList;

public class RssFavListFragmentPresenterImpl implements RssFavListFragmentPresenter, RecyclerView.OnClickListener {

    private Context context;
    private RssFavListFragment fragment;
    private RssFavListFragmentView fragmentView;
    private ArrayList<RssItem> items;

    public RssFavListFragmentPresenterImpl(Context context, RssFavListFragment fragment,
                                           RssFavListFragmentView fragmentView) {
        this.context = context;
        this.fragment = fragment;
        this.fragmentView = fragmentView;
    }

    @Override
    public void onDetach() {
        context = null;
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        Bundle bundle = fragment.getArguments();
        if (bundle != null && bundle.containsKey(RssFavListFragment.RSS_ITEMS_KEY))
            items = bundle.getParcelableArrayList(RssFavListFragment.RSS_ITEMS_KEY);

        if (items == null)
            items = new ArrayList<>();

        fragmentView.setUpRecyclerView(context, new RssRecyclerViewAdapter(context, items, this));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rss_item_container:
                Intent intent = new Intent(context, RssFavDetailsActivity.class);
                intent.putParcelableArrayListExtra(RssFavListFragment.RSS_ITEMS_KEY, items);
                intent.putExtra(RssFavDetailsActivity.RSS_ITEMS_START_POS,
                        fragmentView.getRecyclerViewChildAdapterPosition(view));
                fragment.startActivity(intent);
                break;
        }
    }
}
