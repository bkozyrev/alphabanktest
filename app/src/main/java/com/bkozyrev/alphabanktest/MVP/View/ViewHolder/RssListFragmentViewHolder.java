package com.bkozyrev.alphabanktest.MVP.View.ViewHolder;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

public class RssListFragmentViewHolder {

    public TextView emptyListMessage;
    public RecyclerView list;
    public SwipeRefreshLayout swipeRefreshLayout;

    public RssListFragmentViewHolder(TextView emptyListMessage, RecyclerView list, SwipeRefreshLayout swipeRefreshLayout) {
        this.emptyListMessage = emptyListMessage;
        this.list = list;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }
}
