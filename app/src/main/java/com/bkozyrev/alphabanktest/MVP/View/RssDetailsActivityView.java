package com.bkozyrev.alphabanktest.MVP.View;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.bkozyrev.alphabanktest.Adapters.RssPagerAdapter;

public interface RssDetailsActivityView {

    void setClickListener(View.OnClickListener listener);

    void setUpViewPager(ViewPager.OnPageChangeListener listener, RssPagerAdapter adapter, int startPosition);

    void setFavButton(Context context, boolean isFav);

    boolean isFavBtnActive();

    int getPagerCurrentItem();
}
