package com.bkozyrev.alphabanktest.MVP.View.ViewHolder;

import android.support.v4.view.ViewPager;
import android.widget.ImageButton;

public class RssDetailsActivityViewHolder {

    public ViewPager viewPager;
    public ImageButton favBtn, shareBtn;

    public RssDetailsActivityViewHolder(ViewPager viewPager, ImageButton favBtn, ImageButton shareBtn) {
        this.viewPager = viewPager;
        this.favBtn = favBtn;
        this.shareBtn = shareBtn;
    }
}
