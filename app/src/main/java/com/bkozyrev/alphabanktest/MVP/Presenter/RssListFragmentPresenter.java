package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.os.Bundle;

public interface RssListFragmentPresenter {

    void onDetach();

    void onViewCreated(Bundle savedInstanceState);

    void onDestroy();
}
