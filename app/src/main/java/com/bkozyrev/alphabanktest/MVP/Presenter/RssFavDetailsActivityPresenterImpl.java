package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.bkozyrev.alphabanktest.Adapters.RssPagerAdapter;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssListFragment;
import com.bkozyrev.alphabanktest.MVP.View.RssFavDetailsActivityView;
import com.bkozyrev.alphabanktest.Model.RssItem;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.MVP.Activity.RssFavDetailsActivity;

import java.util.ArrayList;

public class RssFavDetailsActivityPresenterImpl implements RssFavDetailsActivityPresenter {

    private RssFavDetailsActivity activity;
    private Context context;
    private RssFavDetailsActivityView view;
    private ArrayList<RssItem> favItems;

    public RssFavDetailsActivityPresenterImpl(RssFavDetailsActivity activity, Context context,
                                              RssFavDetailsActivityView view) {
        this.activity = activity;
        this.context = context;
        this.view = view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        favItems = new ArrayList<>();
        int startPosition = 0;
        Intent intent = activity.getIntent();
        if (intent != null && intent.hasExtra(RssListFragment.RSS_ITEMS_KEY))
            favItems = intent.getParcelableArrayListExtra(RssListFragment.RSS_ITEMS_KEY);
        if (intent != null && intent.hasExtra(RssFavDetailsActivity.RSS_ITEMS_START_POS))
            startPosition = intent.getIntExtra(RssFavDetailsActivity.RSS_ITEMS_START_POS, 0);

        view.setUpViewPager(new RssPagerAdapter(context, favItems), startPosition);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                activity.onBackPressed();
                break;
        }
        return false;
    }
}
