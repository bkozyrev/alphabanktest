package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.os.Bundle;

public interface SplashActivityPresenter {

    void onCreate(Bundle savedInstanceState);

    void onPause();
}
