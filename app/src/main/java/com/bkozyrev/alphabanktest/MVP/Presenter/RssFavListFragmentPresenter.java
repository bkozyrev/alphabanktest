package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.os.Bundle;

public interface RssFavListFragmentPresenter {

    void onDetach();

    void onViewCreated(Bundle savedInstanceState);
}
