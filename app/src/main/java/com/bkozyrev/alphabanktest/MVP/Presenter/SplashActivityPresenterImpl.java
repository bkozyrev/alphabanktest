package com.bkozyrev.alphabanktest.MVP.Presenter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.bkozyrev.alphabanktest.MVP.Model.SplashActivityModel;
import com.bkozyrev.alphabanktest.MVP.View.SplashActivityView;
import com.bkozyrev.alphabanktest.MVP.Activity.MainActivity;
import com.bkozyrev.alphabanktest.Model.Rss;
import com.bkozyrev.alphabanktest.R;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssListFragment;
import com.bkozyrev.alphabanktest.RssService;
import com.bkozyrev.alphabanktest.MVP.Activity.SplashActivity;
import com.bkozyrev.alphabanktest.Utils.IsoToCountry;
import com.bkozyrev.alphabanktest.Utils.PreferenceHelper;
import com.bkozyrev.alphabanktest.Utils.Utils;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class SplashActivityPresenterImpl implements SplashActivityPresenter, View.OnClickListener {

    private SplashActivity activity;
    private Context context;
    private SplashActivityModel model;
    private SplashActivityView view;
    private Subscription textChangeSubscription, apiSubscription;
    private Rss mRss;

    public SplashActivityPresenterImpl(SplashActivity activity, SplashActivityModel model, SplashActivityView view) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.model = model;
        this.view = view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setClickListener(this);
        setUpTextChangeSubscription();

        if (PreferenceHelper.isFirstTimeOpenApp(context)) {
            view.disableLoginButton();
            setUpApiSubscription();
        } else {
            view.setButtonText(context.getString(R.string.login));
        }

        prepareAlarmManager();
    }

    private void setUpTextChangeSubscription() {
        textChangeSubscription = model
                .changeText()
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String str) {
                        return str.length() < 7;
                    }
                })
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String str) {
                        //Для парсинга кода используется библиотека googlecode.libphonenumber.
                        // Требуется вводить только валидные номера, иначе возможно нераспознавание
                        int phoneNumber = Utils.parsePhoneNumber(context, str);

                        if (phoneNumber == -1)
                            setCountryFlag("");
                        else
                            setCountryFlag(IsoToCountry.getCountry(phoneNumber));
                    }
                });
    }

    private void setUpApiSubscription() {
        view.enableProgressBar();
        apiSubscription = model.request()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Rss>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.disableProgressBar();
                        view.setButtonText(context.getString(R.string.login_error));
                    }

                    @Override
                    public void onNext(Rss rss) {
                        view.disableProgressBar();
                        view.setButtonText(context.getString(R.string.login));
                        view.enableLoginButton();
                        mRss = rss;
                        PreferenceHelper.saveFirstTimeOpenApp(context);
                    }
                });
    }

    private void prepareAlarmManager() {
        long timeWhen = System.currentTimeMillis();

        try {
            //Каждые 10 минут запускается сервис для фоновой отправки запроса на получение новостей.
            //После выполнения работы шлет локальный бродкаст, который ловит список новостейю
            Intent serviceIntent = new Intent(context, RssService.class);

            PendingIntent pendingIntent = PendingIntent.getService(
                    context,
                    0,
                    serviceIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            //Здесь можно также использовать ScheduledExecutorService
            AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            alarm.setRepeating(AlarmManager.RTC_WAKEUP,
                    timeWhen,
                    1000 * 60 * 10, //10 min
                    pendingIntent);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void setCountryFlag(String country) {

        Drawable drawable = null;

        switch (country) {
            case IsoToCountry.RU:
                drawable = ContextCompat.getDrawable(context, R.drawable.ru);
                break;
            case IsoToCountry.BY:
                drawable = ContextCompat.getDrawable(context, R.drawable.by);
                break;
            case IsoToCountry.UA:
                drawable = ContextCompat.getDrawable(context, R.drawable.ua);
                break;
            case IsoToCountry.KG:
                drawable = ContextCompat.getDrawable(context, R.drawable.kg);
                break;
            case IsoToCountry.MD:
                drawable = ContextCompat.getDrawable(context, R.drawable.md);
                break;
            default:
                break;
        }

        view.setFlagDrawable(drawable);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                Intent intent = new Intent(context, MainActivity.class);
                if (mRss != null)
                    intent.putParcelableArrayListExtra(RssListFragment.RSS_ITEMS_KEY, mRss.getChannel().getRssItems());
                activity.startActivity(intent);
                break;
        }
    }

    @Override
    public void onPause() {
        if (textChangeSubscription != null && !textChangeSubscription.isUnsubscribed())
            textChangeSubscription.unsubscribe();

        if (apiSubscription != null && !apiSubscription.isUnsubscribed())
            apiSubscription.unsubscribe();
    }
}
