package com.bkozyrev.alphabanktest.MVP.View.ViewHolder;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;

public class MainActivityViewHolder {

    public DrawerLayout drawerLayout;
    public NavigationView navigationView;
    public Toolbar toolbar;

    public MainActivityViewHolder(DrawerLayout drawerLayout, NavigationView navigationView, Toolbar toolbar) {
        this.drawerLayout = drawerLayout;
        this.navigationView = navigationView;
        this.toolbar = toolbar;
    }
}
