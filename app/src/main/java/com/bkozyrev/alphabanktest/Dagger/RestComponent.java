package com.bkozyrev.alphabanktest.Dagger;

import com.bkozyrev.alphabanktest.RssService;
import com.bkozyrev.alphabanktest.MVP.Activity.SplashActivity;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssListFragment;
import com.bkozyrev.alphabanktest.Server.RestModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = RestModule.class)
public interface RestComponent {
    void inject(SplashActivity splashActivity);
    void inject(RssListFragment rssListFragment);
    void inject(RssService rssService);
}
