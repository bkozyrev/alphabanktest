package com.bkozyrev.alphabanktest.Dagger;

import lombok.Synchronized;

public class InjectorClass {

    private static RestComponent restComponent;

    @Synchronized
    public static RestComponent getRestComponent(){
        if(restComponent == null){
            restComponent = DaggerRestComponent.create();
        }
        return restComponent;
    }
}
