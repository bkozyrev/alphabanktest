package com.bkozyrev.alphabanktest.Utils;

import android.content.Context;
import android.util.Log;

import com.bkozyrev.alphabanktest.R;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

public class Utils {

    private static final String LOG_TAG = Utils.class.getSimpleName();

    public static int parsePhoneNumber(Context context, String phoneNumber) {

        //Только для российских номеров (Указано в задании)
        if (phoneNumber.startsWith("8"))
            return 8;

        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        int countryCode = -1;
        try {
            countryCode = phoneNumberUtil.parse(phoneNumber, "").getCountryCode();
        } catch (NumberParseException e) {
            Log.d(LOG_TAG, context.getString(R.string.phone_parse_error));
        }

        Log.d(LOG_TAG, "countryCode = " + countryCode);

        return countryCode;
    }
}
