package com.bkozyrev.alphabanktest.Utils;

import android.app.Application;

import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

public class AlphaBankTestApplication extends Application {

    private static final String APP_ID = "1270158796366966";
    private static final String APP_NAMESPACE = "alpha_bank_test";

    @Override
    public void onCreate() {
        super.onCreate();

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(APP_ID)
                .setNamespace(APP_NAMESPACE)
                .build();

        SimpleFacebook.setConfiguration(configuration);
    }
}
