package com.bkozyrev.alphabanktest.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.bkozyrev.alphabanktest.Model.RssItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class PreferenceHelper {

    private static final String PREFS_NAME = "shared_prefs";
    private static final String PREFS_FAVORITES = "favorites";
    private static final String PREFS_FIRST_TIME_OPEN = "first_time_open";

    public static void saveRssItemToFav(Context context, RssItem item){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String inJson = sharedPreferences.getString(PREFS_FAVORITES, "");
        Gson gson = new Gson();
        ArrayList<RssItem> items = new ArrayList<>();
        if (!TextUtils.isEmpty(inJson)) {
            items = gson.fromJson(inJson, new TypeToken<List<RssItem>>(){}.getType());
        }
        items.add(item);
        String outJson = gson.toJson(items);
        editor.putString(PREFS_FAVORITES, outJson);
        editor.apply();
    }

    public static ArrayList<RssItem> getFavList(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        return gson.fromJson(sharedPreferences.getString(PREFS_FAVORITES, ""), new TypeToken<List<RssItem>>(){}.getType());
    }

    public static void removeRssItemFromFav(Context context, RssItem item) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String inJson = sharedPreferences.getString(PREFS_FAVORITES, "");
        Gson gson = new Gson();
        ArrayList<RssItem> items = new ArrayList<>();
        if (!TextUtils.isEmpty(inJson)) {
            items = gson.fromJson(inJson, new TypeToken<List<RssItem>>(){}.getType());
        }
        items.remove(item);
        String outJson = gson.toJson(items);
        editor.putString(PREFS_FAVORITES, outJson);
        editor.apply();
    }

    public static boolean isFirstTimeOpenApp(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(PREFS_FIRST_TIME_OPEN, true);
    }

    public static void saveFirstTimeOpenApp(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREFS_FIRST_TIME_OPEN, false);
        editor.apply();
    }
}
