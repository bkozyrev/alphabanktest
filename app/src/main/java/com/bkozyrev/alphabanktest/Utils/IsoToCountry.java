package com.bkozyrev.alphabanktest.Utils;

import java.util.HashMap;
import java.util.Map;

public class IsoToCountry {

    public static final String RU = "RU";
    public static final String BY = "BY";
    public static final String UA = "UA";
    public static final String KG = "KG";
    public static final String MD = "MD";

    private static Map<Integer, String> codeToCountry = new HashMap<>();

    static {
        codeToCountry.put(7, RU);
        codeToCountry.put(8, RU);
        codeToCountry.put(375, BY);
        codeToCountry.put(380, UA);
        codeToCountry.put(996, KG);
        codeToCountry.put(373, MD);
    }

    public static String getCountry(int code) {
        if (codeToCountry.containsKey(code))
            return codeToCountry.get(code);
        else
            return "";
    }
}
