package com.bkozyrev.alphabanktest.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.bkozyrev.alphabanktest.Model.RssItem;
import com.bkozyrev.alphabanktest.R;

import java.util.ArrayList;

public class RssPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<RssItem> mItems;
    private LayoutInflater mInflater;

    public RssPagerAdapter(Context context, ArrayList<RssItem> items) {
        this.mContext = context;
        this.mItems = items;
        this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View viewLayout;
        viewLayout = mInflater.inflate(R.layout.item_rss_details, container, false);

        WebView description = (WebView) viewLayout.findViewById(R.id.rss_description);

        if (mItems.get(position) != null) {
            WebSettings settings = description.getSettings();
            settings.setDefaultTextEncodingName("utf-8");
            description.loadData(mItems.get(position).getDescription(), "text/html; charset=utf-8", "UTF-8");
        }

        container.addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
