package com.bkozyrev.alphabanktest.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bkozyrev.alphabanktest.Model.RssItem;
import com.bkozyrev.alphabanktest.R;

import java.util.ArrayList;

public class RssRecyclerViewAdapter extends RecyclerView.Adapter<RssRecyclerViewAdapter.RssViewHolder> {

    private Context mContext;
    private ArrayList<RssItem> mItems;
    private LayoutInflater mInflater;
    private RecyclerView.OnClickListener mClickListener;

    public RssRecyclerViewAdapter(Context context, ArrayList<RssItem> items, RecyclerView.OnClickListener listener) {
        this.mContext = context;
        this.mItems = items;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mClickListener = listener;
    }

    @Override
    public RssViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RssViewHolder(mInflater.inflate(R.layout.item_rss_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RssViewHolder holder, int position) {
        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void updateRss(ArrayList<RssItem> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public class RssViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView mTitle;

        public RssViewHolder(View itemView) {
            super(itemView);

            mTitle = (AppCompatTextView) itemView.findViewById(R.id.rss_title);

            itemView.setOnClickListener(mClickListener);
        }

        public void bind(RssItem rssItem) {
            if (rssItem.getTitle() != null)
                mTitle.setText(rssItem.getTitle());
        }
    }
}
