package com.bkozyrev.alphabanktest;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.bkozyrev.alphabanktest.Dagger.InjectorClass;
import com.bkozyrev.alphabanktest.MVP.Fragment.RssListFragment;
import com.bkozyrev.alphabanktest.Model.Rss;
import com.bkozyrev.alphabanktest.Model.RssItem;
import com.bkozyrev.alphabanktest.Server.AlphaAPI;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RssService extends Service {

    public static final String ACTION_RSS_UPDATE = "rss_update_action";

    private Subscription mApiSubscription;

    @Inject
    AlphaAPI alphaAPI;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        InjectorClass.getRestComponent().inject(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mApiSubscription = alphaAPI.getRss()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Rss>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        stopSelf();
                    }

                    @Override
                    public void onNext(Rss rss) {
                        if (rss != null)
                            sendLocalBroadcast(rss.getChannel().getRssItems());

                        stopSelf();
                    }
                });

        return START_STICKY;
    }

    private void sendLocalBroadcast(ArrayList<RssItem> rssItems) {
        //Локальный бродкаст обрабатывается в RssListFragmentPresenterImpl
        Intent intent = new Intent(ACTION_RSS_UPDATE);
        intent.putParcelableArrayListExtra(RssListFragment.RSS_ITEMS_KEY, rssItems);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        if (mApiSubscription != null && !mApiSubscription.isUnsubscribed())
            mApiSubscription.unsubscribe();

        super.onDestroy();
    }
}
